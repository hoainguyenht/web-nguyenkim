<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="bootstrap-3.4.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="jquery.min.js"></script>
    <script src="bootstrap-3.4.1-dist/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
    <!-- <script src="custom.js"></script> -->
</head>

<body>
    <header>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="logo">
                            <a href="index.php">
                                <img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2017/T11/homepage/Logo_NK.svg?v=2020" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="search-box">
                            <input type="text" placeholder="Bạn cần tìm gì hôm nay ?">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="hotline pull-right">
                            <a href="tel:18006800">
                                <div class="icon">
                                    <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                                </div>
                                <p>
                                    <span>Hotline: 1800 6800 (Miễn phí)</span>
                                    <span>Mua hàng - Góp ý - Bảo hành </span>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="category">
                            <h3><i class="fa fa-bars" aria-hidden="true"></i>Danh mục sản phẩm</h3>
                            <ul class="nav-menu">
                                <li>
                                    <a href="#"><i class="fa fa-television" aria-hidden="true"></i>Tivi</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Tivi</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Sony</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>LG</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sharp</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Electrolux</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Toshiba</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Panasonic</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Loại Tivi</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Android TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Smart TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Internet TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>OLED TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>QLED TV</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Kích thước</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Trên 50 inch</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Từ 44 - 55 inch</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Từ 32 - 43 inch</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#"><i class="fridge"></i>Tủ lạnh</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thương hiệu</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>LQ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Toshiba</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>AQUA</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Panasonic</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Electrolux</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sharp</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Loại tủ lạnh</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Multi doors</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Side by side</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ngăn đá trên</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ngăn đá dưới</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Mini</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Dung tích</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Dưới 150 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>150 - 300 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>300 - 450 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Trên 450 lít</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-laptop" aria-hidden="true"></i>Máy tính</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Laptop</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Dell</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Acer</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Asus</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Apple</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>MSI</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lenovo</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Màn hình LCD</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>DELL</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>ASUS</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>LG</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>SAMSUNG</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Loa bluetooth</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Chuột máy tính</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bàn phím</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>USB</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ổ cứng SSD/HDD</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Tai nghe</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-print" aria-hidden="true"></i>Máy in</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Máy in</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Canon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Epson</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Brother</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thiết bị văn phòng</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy FAX</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy photocopy</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy chiếu</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy tính</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy Scan</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Mực in</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Màn chiếu</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#"><i class="camera"></i>Camera giám sát</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thương hiệu</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Canon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sony</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Nikon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>GoPro</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Benro</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Wasabi</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Camera giám sát</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Action camera</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy bay camera - drone</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Camera hành trình</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Camera quan sát</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Webcam</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Thẻ nhớ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Pin - sạc</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="#"><i class="boiler"></i>Thiết bị điện nước</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Gia dụng nhà bếp</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Nồi cơm điện</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bình đun siêu tốc</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lò vi sóng</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bếp từ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lò nướng</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thiết bị gia đình</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy lọc nước</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy hút bụi</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy lọc không khí</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy nước nóng</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy quạt</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="menu-service">
                            <ul>
                                <li><a href="#"><i class="sale"></i>Tổng hợp khuyến mãi</a></li>
                                <li><a href="#"><i class="trial-free"></i>Miễn phí dùng thử</a></li>
                                <li><a href="#"><i class="guarantee"></i>Tặng 1 năm bảo hành</a></li>
                                <li><a href="#"><i class="return"></i>1 đổi 1 trong 1 năm</a></li>
                            </ul>
                        </div>
                        <div class="slideshow">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class=""></li>
                                    <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                                    <li data-target="#myCarousel" data-slide-to="2" class="active"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item">
                                        <img src="https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/0320/ChaoHeMatLanh/Main/1008X405.jpg" alt="">
                                    </div>

                                    <div class="item">
                                        <img src="https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/0320/uudaitaichinh/v1/1008X405.jpg" alt="">
                                    </div>

                                    <div class="item active">
                                        <img src="https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/0320/NangCaoSucKhoe_Corona/Heathy_Fair_1008x405.jpg" alt="">
                                    </div>
                                </div>

                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <section id="new-product">
            <div class="container">
                <div class="info-product">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/588/10041987_TIVI_SONY_KD-55X7000G_VN3_01.jpg" alt="">
                                    <span class="position-top-right">
                                            <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Smart Tivi Sony 4K 55 inch KD-55X7000G VN3</p>
                                        <p class="price">13.300.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/574/10038180_TULANH_HITACHI_R-FW690PGV7-GBK_01.jpg" alt="">
                                    <span class="position-top-right">
                                            <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Tủ lạnh Hitachi R-FW690PGV7 (GBK)</p>
                                        <p class="price">28.800.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/273/10033030-MG-LG-FC1409S2E-01.png" alt="">
                                    <span class="position-top-right">
                                            <img src="" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Máy giặt LG 9 kg FC1409S2E</p>
                                        <p class="price">12.990.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Miễn phí vận chuyển </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/289/DH-3RP2.jpg" alt="">
                                    <span class="position-top-right">
                                            <img src="https://cdn.nguyenkimmall.com/images/companies/_1/Data_Price/2020/doc_quyen_online.png" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Máy nước nóng Panasonic DH-3RP2</p>
                                        <p class="price">3.890.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Tặng Chảo Elmich </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/588/10041987_TIVI_SONY_KD-55X7000G_VN3_01.jpg" alt="">
                                    <span class="position-top-right">
                                            <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Smart Tivi Sony 4K 55 inch KD-55X7000G VN3</p>
                                        <p class="price">13.300.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/574/10038180_TULANH_HITACHI_R-FW690PGV7-GBK_01.jpg" alt="">
                                    <span class="position-top-right">
                                            <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Tủ lạnh Hitachi R-FW690PGV7 (GBK)</p>
                                        <p class="price">28.800.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/273/10033030-MG-LG-FC1409S2E-01.png" alt="">
                                    <span class="position-top-right">
                                            <img src="" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Máy giặt LG 9 kg FC1409S2E</p>
                                        <p class="price">12.990.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Miễn phí vận chuyển </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="item">
                                <a href="product.php">
                                    <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/289/DH-3RP2.jpg" alt="">
                                    <span class="position-top-right">
                                            <img src="https://cdn.nguyenkimmall.com/images/companies/_1/Data_Price/2020/doc_quyen_online.png" alt="">
                                        </span>
                                    <div class="desc-product">
                                        <p class="name">Máy nước nóng Panasonic DH-3RP2</p>
                                        <p class="price">3.890.000đ</p>
                                        <p class="price-custom">Trả góp 0%</p>
                                        <p class="promotion"> Tặng Chảo Elmich </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <footer>
        <div class="container">
            <div class="info-footer">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="info-comany">
                            <li class="title">Thông tin công ty</li>
                            <li><a href="#">Giới thiệu công ty</a></li>
                            <li><a href="#">Đối tác chiến lược</a></li>
                            <li><a href="#">Hệ thống trung tâm</a></li>
                            <li><a href="#">Tuyển dụng</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="policy">
                            <li class="title">Chính sách</li>
                            <li><a href="#">Giao nhận - lắp đặt</a></li>
                            <li><a href="#">Dùng thử sản phẩm</a></li>
                            <li><a href="#">Đổi trả sản phẩm</a></li>
                            <li><a href="#">Bảo hành</a></li>
                            <li><a href="#">Trả góp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="support">
                            <li class="title">Hỗ trợ khách hàng</li>
                            <li><a href="#">Hướng dẫn mua hàng</a></li>
                            <li><a href="#">Tra cứu bảo hành</a></li>
                            <li><a href="#">Hóa đơn điện tử</a></li>
                            <li><a href="#">Mua hàng doanh nghiệp</a></li>
                            <li><a href="#">Câu hỏi thường gặp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="contact-us">
                            <li class="title">Kết nối với chúng tôi</li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script>
        $('.category h3 i').click(function() {
        $('.nav-menu').toggleClass('active');
    });
    </script>
</body>

</html>