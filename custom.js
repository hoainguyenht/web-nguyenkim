$(document).ready(function() {

    $(".info-product-top").owlCarousel({

        autoPlay: false,
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        center: true,
        navigation: true,
        navigationText: ['<span ><i class="fa fa-chevron-left "></i></span>',
            '<span ><i class="fa fa-chevron-right "></i></span>'
        ],
        loop: true,
        responsive: {
            600: {
                items: 4
            }
        }
    });
    $(".info-product").owlCarousel({

        autoPlay: false,
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        center: true,
        navigation: true,
        navigationText: ['<span ><i class="fa fa-chevron-left "></i></span>',
            '<span ><i class="fa fa-chevron-right "></i></span>'
        ],
        loop: true,
        responsive: {
            600: {
                items: 4
            }
        }
    });
    $(".banner-product").owlCarousel({
        autoPlay: false,
        items: 2,
        center: true,
        navigation: true,
        navigationText: ['<span ><i class="fa fa-chevron-left "></i></span>',
            '<span ><i class="fa fa-chevron-right "></i></span>'
        ],
        loop: true,
    });

    // toggle menu
    $('.category h3 i').click(function() {
        $('.nav-menu').toggleClass('active');
    });
});