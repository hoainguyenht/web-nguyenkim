<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="bootstrap-3.4.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="jquery.min.js"></script>
    <script src="bootstrap-3.4.1-dist/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
    <!-- <script src="custom.js"></script> -->
</head>

<body class="page-category">
    <header>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="logo">
                            <a href="index.php">
                                <img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2017/T11/homepage/Logo_NK.svg?v=2020" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="search-box">
                            <input type="text" placeholder="Bạn cần tìm gì hôm nay ?">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="hotline pull-right">
                            <a href="tel:18006800">
                                <div class="icon">
                                    <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                                </div>
                                <p>
                                    <span>Hotline: 1800 6800 (Miễn phí)</span>
                                    <span>Mua hàng - Góp ý - Bảo hành </span>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="category">
                            <h3><i class="fa fa-bars" aria-hidden="true"></i>Danh mục sản phẩm</h3>
                            <ul class="nav-menu">
                                <li>
                                    <a href="category.php"><i class="fa fa-television" aria-hidden="true"></i>Tivi</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Tivi</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Sony</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>LG</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sharp</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Electrolux</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Toshiba</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Panasonic</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Loại Tivi</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Android TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Smart TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Internet TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>OLED TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>QLED TV</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Kích thước</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Trên 50 inch</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Từ 44 - 55 inch</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Từ 32 - 43 inch</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="fridge"></i>Tủ lạnh</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thương hiệu</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>LQ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Toshiba</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>AQUA</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Panasonic</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Electrolux</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sharp</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Loại tủ lạnh</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Multi doors</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Side by side</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ngăn đá trên</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ngăn đá dưới</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Mini</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Dung tích</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Dưới 150 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>150 - 300 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>300 - 450 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Trên 450 lít</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="fa fa-laptop" aria-hidden="true"></i>Máy tính</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Laptop</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Dell</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Acer</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Asus</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Apple</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>MSI</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lenovo</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Màn hình LCD</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>DELL</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>ASUS</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>LG</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>SAMSUNG</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Loa bluetooth</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Chuột máy tính</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bàn phím</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>USB</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ổ cứng SSD/HDD</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Tai nghe</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="fa fa-print" aria-hidden="true"></i>Máy in</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Máy in</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Canon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Epson</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Brother</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thiết bị văn phòng</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy FAX</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy photocopy</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy chiếu</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy tính</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy Scan</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Mực in</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Màn chiếu</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="camera"></i>Camera giám sát</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thương hiệu</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Canon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sony</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Nikon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>GoPro</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Benro</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Wasabi</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Camera giám sát</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Action camera</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy bay camera - drone</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Camera hành trình</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Camera quan sát</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Webcam</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Thẻ nhớ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Pin - sạc</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="boiler"></i>Thiết bị điện nước</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Gia dụng nhà bếp</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Nồi cơm điện</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bình đun siêu tốc</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lò vi sóng</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bếp từ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lò nướng</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thiết bị gia đình</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy lọc nước</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy hút bụi</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy lọc không khí</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy nước nóng</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy quạt</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="menu-service">
                            <ul>
                                <li><a href="#"><i class="sale"></i>Tổng hợp khuyến mãi</a></li>
                                <li><a href="#"><i class="trial-free"></i>Miễn phí dùng thử</a></li>
                                <li><a href="#"><i class="guarantee"></i>Tặng 1 năm bảo hành</a></li>
                                <li><a href="#"><i class="return"></i>1 đổi 1 trong 1 năm</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="slide">
        <div class="item">
            <img src="https://www.nguyenkim.com/images/companies/_1/MKT_ECM/0320/ChaoHeMatLanh/Sub_KV/Tivi_Tulanh/xCategory_TV_1920x250,P20,282,29.jpg.pagespeed.ic.POm3WERL36.webp" alt="" width="100%">
        </div>
    </section>
    <section id="description">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Trang chủ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Danh mục</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="description">
                        <h2>Category</h2>
                        <div class="full">
                            <p>Bạn đang tìm một chiếc tivi cho căn nhà mới tậu của nhà mình? Bạn cần tìm một thương hiệu tivi xịn sò để chọn làm quà tặng người thân? Hay chỉ đơn giản là thay chiếc tivi cũ dùng đã lâu nhưng chưa biết chọn loại nào? Tham khảo
                                những tiêu chí chọn tivi dưới đây để tìm được sản phẩm mình mong muốn nhất nhé!</p>
                            <p><span><strong>Khoảng cách, </strong></span><span><strong>kích thước</strong></span><strong>:</strong></p>
                            <p>- 1.8 – 2.4m: Dưới 44 inch</p>
                            <p>- 2.7 – 3.2m: 55 - 65 inch</p>
                            <p>- 4m trở lên: Trên 75 inch</p>
                            <p><span><strong>Độ phân giải</strong></span><strong>:</strong></p>
                            <p>- Dưới 32 inch: <a href="https://www.nguyenkim.com/tivi-man-hinh-lcd/?sort_by=position&amp;sort_order=desc&amp;features_hash=77-5363-51096-84158-68256-82522-50360">Tivi HD</a></p>
                            <p>- Từ 32 - 40 inch: <a href="https://www.nguyenkim.com/tivi-man-hinh-lcd/?sort_by=position&amp;sort_order=desc&amp;features_hash=77-5341-84182-85768-83070">Tivi Full HD</a></p>
                            <p>- Trên 40 inch: <a href="https://www.nguyenkim.com/tivi-man-hinh-lcd/?sort_by=position&amp;sort_order=desc&amp;features_hash=77-82834-43821-50422-84042-22015-84752-85666-81884-69824-66082-49350-83210-80652">Tivi 4K</a> trải
                                nghiệm công nghệ đỉnh cao hình ảnh</p>
                            <p><strong>Loại Tivi:</strong> Nếu nhà bạn có người lớn tuổi, chỉ cần xem truyền hình cơ bản, bạn có thể chọn tivi thường tiết kiệm chi phí. Ngoài ra, bạn có thể chọn <a href="https://www.nguyenkim.com/tivi-man-hinh-lcd/?features_hash=36-150347-82928">Smart Tivi</a>                                nếu có nhu cầu chơi game, xem Youtube, Netflix, nghe nhạc trên Spotify.</p>
                            <p><strong>Thương hiệu và giá thành: </strong>Tại Nguyễn Kim có nhiều thương hiệu Tivi phổ biến cho khách hàng dễ dàng lựa chọn như <a href="https://www.nguyenkim.com/tivi-lcd-samsung/">Samsung</a>, <a href="https://www.nguyenkim.com/tivi-lcd-lg/">LG</a>,
                                <a href="https://www.nguyenkim.com/tivi-lcd-sony/">Sony</a>, <a href="https://www.nguyenkim.com/tivi-lcd-toshiba/">Toshiba</a>, <a href="https://www.nguyenkim.com/tivi-lcd-tcl/">TCL</a>,… Ngoài được mua những sản phẩm chính
                                hãng với giá tốt nhất trên thị trường, Nguyễn Kim còn mang đến nhiều <span><a href="https://www.nguyenkim.com/chinh-sach-dich-vu-khach-hang-vuot-troi.html">dịch vụ độc quyền</a></span> như <a href="https://www.nguyenkim.com/khuyen-mai-tra-gop-tai-nguyen-kim.html">trả góp 0%</a>                                miễn phí giao hàng, 1 tuần dùng thử, tặng 1 năm bảo hành, lỗi 1 đổi 1 trong 1 năm tại toàn bộ các trung tâm phủ sóng toàn quốc.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="banner">
                        <div class="widget widget-item">
                            <h2>Bộ lọc danh mục sản phẩm</h2>
                            <p>Giúp lọc nhanh sản phẩm bạn tìm kiếm</p>
                        </div>
                        <div class="widget widget-trademark">
                            <h4>Thương hiệu</h4>
                            <ul>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/65/30/feature_variant/299/Sony_8eun-u1.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/75/30/feature_variant/299/Samsung_r4o3-uh.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/38/30/feature_variant/299/LG_2el0-h6.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/64/30/feature_variant/299/Phillip_w92h-or.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/40/30/feature_variant/299/TCL_2owi-9h.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/67/30/feature_variant/299/toshibar_yksk-1d.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/78/30/feature_variant/299/panasonic_t1z4-1b.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <img src="https://cdn.nguyenkimmall.com/images/thumbnails/35/30/feature_variant/590/Viettronics-TB__2__oahs-jd.png" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="widget widget-price">
                            <h4>Giá bán</h4>
                            <ul>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <p>Dưới 5 triệu</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <p>Từ 5 - 7 triệu</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <p>Từ 7 - 10 triệu</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="checkbox">
                                        <p>Trên 10 triệu</p>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="content">
                        <div class="sort-cat">
                            <h3>Sắp xếp theo: </h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <input type="radio">
                                        <p>Quan tâm nhất</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="radio">
                                        <p>Giá thấp đến cao</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="radio">
                                        <p>Giá cao xuống thấp</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <input type="radio">
                                        <p>Bán chạy nhất</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="product-cat">
                            <div class="info-product">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item" style="margin-left: 15px;">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/588/10041987_TIVI_SONY_KD-55X7000G_VN3_01.jpg" alt="">
                                                <span class="position-top-right">
                                                        <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Smart Tivi Sony 4K 55 inch KD-55X7000G VN3</p>
                                                    <p class="price">13.300.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/574/10038180_TULANH_HITACHI_R-FW690PGV7-GBK_01.jpg" alt="">
                                                <span class="position-top-right">
                                                        <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Tủ lạnh Hitachi R-FW690PGV7 (GBK)</p>
                                                    <p class="price">28.800.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/273/10033030-MG-LG-FC1409S2E-01.png" alt="">
                                                <span class="position-top-right">
                                                        <img src="" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Máy giặt LG 9 kg FC1409S2E</p>
                                                    <p class="price">12.990.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Miễn phí vận chuyển </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item" style="margin-right: 15px;">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/289/DH-3RP2.jpg" alt="">
                                                <span class="position-top-right">
                                                        <img src="https://cdn.nguyenkimmall.com/images/companies/_1/Data_Price/2020/doc_quyen_online.png" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Máy nước nóng Panasonic DH-3RP2</p>
                                                    <p class="price">3.890.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Tặng Chảo Elmich </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item" style="margin-left: 15px;">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/588/10041987_TIVI_SONY_KD-55X7000G_VN3_01.jpg" alt="">
                                                <span class="position-top-right">
                                                        <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Smart Tivi Sony 4K 55 inch KD-55X7000G VN3</p>
                                                    <p class="price">13.300.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/574/10038180_TULANH_HITACHI_R-FW690PGV7-GBK_01.jpg" alt="">
                                                <span class="position-top-right">
                                                        <img src="https://www.nguyenkim.com/images/companies/_1/Data_Price/2020/xMastercard_1000k.png.pagespeed.ic.Sm8AKmjQkM.webp" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Tủ lạnh Hitachi R-FW690PGV7 (GBK)</p>
                                                    <p class="price">28.800.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Giảm thêm 1tr qua MasterCard </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/273/10033030-MG-LG-FC1409S2E-01.png" alt="">
                                                <span class="position-top-right">
                                                        <img src="" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Máy giặt LG 9 kg FC1409S2E</p>
                                                    <p class="price">12.990.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Miễn phí vận chuyển </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="item" style="margin-right: 15px;">
                                            <a href="product.php">
                                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/180/180/detailed/289/DH-3RP2.jpg" alt="">
                                                <span class="position-top-right">
                                                        <img src="https://cdn.nguyenkimmall.com/images/companies/_1/Data_Price/2020/doc_quyen_online.png" alt="">
                                                    </span>
                                                <div class="desc-product">
                                                    <p class="name">Máy nước nóng Panasonic DH-3RP2</p>
                                                    <p class="price">3.890.000đ</p>
                                                    <p class="price-custom">Trả góp 0%</p>
                                                    <p class="promotion"> Tặng Chảo Elmich </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="info-footer">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="info-comany">
                            <li class="title">Thông tin công ty</li>
                            <li><a href="#">Giới thiệu công ty</a></li>
                            <li><a href="#">Đối tác chiến lược</a></li>
                            <li><a href="#">Hệ thống trung tâm</a></li>
                            <li><a href="#">Tuyển dụng</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="policy">
                            <li class="title">Chính sách</li>
                            <li><a href="#">Giao nhận - lắp đặt</a></li>
                            <li><a href="#">Dùng thử sản phẩm</a></li>
                            <li><a href="#">Đổi trả sản phẩm</a></li>
                            <li><a href="#">Bảo hành</a></li>
                            <li><a href="#">Trả góp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="support">
                            <li class="title">Hỗ trợ khách hàng</li>
                            <li><a href="#">Hướng dẫn mua hàng</a></li>
                            <li><a href="#">Tra cứu bảo hành</a></li>
                            <li><a href="#">Hóa đơn điện tử</a></li>
                            <li><a href="#">Mua hàng doanh nghiệp</a></li>
                            <li><a href="#">Câu hỏi thường gặp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="contact-us">
                            <li class="title">Kết nối với chúng tôi</li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script>
        $('.category h3 i').click(function() {
            $('.nav-menu').toggleClass('active');
        });
    </script>
</body>

</html>