<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="bootstrap-3.4.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="jquery.min.js"></script>
    <script src="bootstrap-3.4.1-dist/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
    <!-- <script src="custom.js"></script> -->
</head>

<body class="page-category">
    <header>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="logo">
                            <a href="index.php">
                                <img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2017/T11/homepage/Logo_NK.svg?v=2020" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="search-box">
                            <input type="text" placeholder="Bạn cần tìm gì hôm nay ?">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="hotline pull-right">
                            <a href="tel:18006800">
                                <div class="icon">
                                    <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                                </div>
                                <p>
                                    <span>Hotline: 1800 6800 (Miễn phí)</span>
                                    <span>Mua hàng - Góp ý - Bảo hành </span>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="category">
                            <h3><i class="fa fa-bars" aria-hidden="true"></i>Danh mục sản phẩm</h3>
                            <ul class="nav-menu">
                                <li>
                                    <a href="category.php"><i class="fa fa-television" aria-hidden="true"></i>Tivi</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Tivi</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Sony</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>LG</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sharp</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Electrolux</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Toshiba</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Panasonic</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Loại Tivi</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Android TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Smart TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Internet TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>OLED TV</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>QLED TV</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Kích thước</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Trên 50 inch</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Từ 44 - 55 inch</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Từ 32 - 43 inch</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="fridge"></i>Tủ lạnh</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thương hiệu</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>LQ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Toshiba</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>AQUA</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Panasonic</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Electrolux</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sharp</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Loại tủ lạnh</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Multi doors</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Side by side</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ngăn đá trên</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ngăn đá dưới</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Mini</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Dung tích</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Dưới 150 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>150 - 300 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>300 - 450 lít</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Trên 450 lít</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="fa fa-laptop" aria-hidden="true"></i>Máy tính</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Laptop</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Dell</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Acer</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Asus</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Apple</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>MSI</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lenovo</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Màn hình LCD</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>DELL</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>ASUS</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>LG</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>SAMSUNG</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Loa bluetooth</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Chuột máy tính</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bàn phím</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>USB</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Ổ cứng SSD/HDD</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Tai nghe</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="fa fa-print" aria-hidden="true"></i>Máy in</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Máy in</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>HP</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Canon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Epson</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Brother</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thiết bị văn phòng</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy FAX</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy photocopy</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy chiếu</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy tính</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy Scan</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Mực in</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Màn chiếu</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="camera"></i>Camera giám sát</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thương hiệu</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Canon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Samsung</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Sony</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Nikon</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>GoPro</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Benro</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Wasabi</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Camera giám sát</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Action camera</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy bay camera - drone</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Camera hành trình</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Camera quan sát</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Webcam</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Phụ kiện</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Thẻ nhớ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Pin - sạc</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="category.php"><i class="boiler"></i>Thiết bị điện nước</a>
                                    <i class="fa fa-chevron-right"></i>
                                    <div class="sub-menu">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Gia dụng nhà bếp</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Nồi cơm điện</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bình đun siêu tốc</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lò vi sóng</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Bếp từ</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Lò nướng</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <h3>Thiết bị gia đình</h3>
                                                    <ul>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy lọc nước</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy hút bụi</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy lọc không khí</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy nước nóng</a></li>
                                                        <li><a href="#"><i class="right-chevron"></i>Máy quạt</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="menu-service">
                            <ul>
                                <li><a href="#"><i class="sale"></i>Tổng hợp khuyến mãi</a></li>
                                <li><a href="#"><i class="trial-free"></i>Miễn phí dùng thử</a></li>
                                <li><a href="#"><i class="guarantee"></i>Tặng 1 năm bảo hành</a></li>
                                <li><a href="#"><i class="return"></i>1 đổi 1 trong 1 năm</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="container">
            <div class="row">
                <div class="product-info">
                    <div class="col-md-6">
                        <div class="product-image">
                            <div class="simple-gallery">

                                <img class="maxi" src="https://cdn.nguyenkimmall.com/images/thumbnails/600/450/detailed/297/10029435_TULANH_SHARP_INVERTER-253-LIT_SJ-X281E-SL_01.jpg">

                                <div class="mini">
                                    <ul>
                                        <li>
                                            <img src="https://cdn.nguyenkimmall.com/images/thumbnails/600/450/detailed/297/10029435_TULANH_SHARP_INVERTER-253-LIT_SJ-X281E-SL_01.jpg">
                                        </li>
                                        <li>
                                            <img src="https://cdn.nguyenkimmall.com/images/thumbnails/600/450/detailed/297/10029435_TULANH_SHARP_INVERTER-253-LIT_SJ-X281E-SL_02.jpg">
                                        </li>
                                        <li>
                                            <img src="https://cdn.nguyenkimmall.com/images/thumbnails/600/450/detailed/297/10029435_TULANH_SHARP_INVERTER-253-LIT_SJ-X281E-SL_03.jpg">
                                        </li>
                                        <li>
                                            <img src="https://cdn.nguyenkimmall.com/images/thumbnails/600/450/detailed/297/10029435_TULANH_SHARP_INVERTER-253-LIT_SJ-X281E-SL_04.jpg" alt="">
                                        </li>
                                        <li>
                                            <img src="https://cdn.nguyenkimmall.com/images/thumbnails/600/450/detailed/534/10029435_TU_LANH_SHARP_INVERTER_253_LIT_SJ_X281E.jpg" alt="">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-info-desc">
                                <h2>Đặc điểm nổi bậc</h2>
                                <ul>
                                    <li>Dung tích tổng: 271 lít - Thực: 253 lít</li>
                                    <li>Kiểu tủ: Ngăn đá trên - 2 cửa</li>
                                    <li>Công nghệ Inverter</li>
                                    <li>Khay kính chịu lực</li>
                                    <li>Xuất xứ: Thái Lan</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product-desc">
                            <div class="product-desc-top">
                                <div class="pro-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="index.php">Trang chủ</a></li>
                                            <li class="breadcrumb-item"><a href="category.php">Danh mục</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Tủ lạnh Sharp</li>
                                        </ol>
                                    </nav>
                                </div>
                                <div class="product-name">
                                    <h1>TỦ LẠNH SHARP INVERTER 253 LÍT SJ-X281E-SL</h1>
                                </div>
                                <div class="pro-btn-share">
                                    <a href="#"><i class="fa fa-share-alt"></i>Share</a>
                                </div>
                            </div>
                            <div class="product-desc-main">
                                <div class="product-price">
                                    <span>6.590.000 đ</span>
                                    <small>Trả góp 0%</small>
                                </div>
                                <div class="product-service">
                                    <a href="#">
                                        <img src="https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/2020_bannerPDP/pdp_SUV-3icon_1.png" alt="" width="100%">
                                    </a>
                                </div>
                                <div class="product-buy">
                                    <div class="buynow">
                                        <h3>mua ngay</h3>
                                        <p>Giao hàng tận nơi</p>
                                    </div>
                                    <div class="tragop">
                                        <h3>mua trả góp</h3>
                                        <p>Chỉ có 1.098.333đ/tháng (6 tháng)</p>
                                    </div>
                                </div>
                                <div class="product-gift">
                                    <h2>ƯU ĐÃI CHỈ CÓ TẠI NGUYỄN KIM, GỌI NGAY
                                        <a href="tel:18006800">1800.6800 </a>ĐỂ ĐẶT HÀNG
                                    </h2>
                                    <ul>
                                        <li><i class="gift-green"></i>Nhập mã NKBEST nhận ưu đãi đặc biệt chỉ từ 19-31/03.<a href="#">Xem chi tiết</a></li>
                                        <li><i class="gift-green"></i>Khách hàng chọn 01 trong 02 ưu đãi:
                                            <label>
                                                <input type="radio" checked="checked"> Giảm ngay 200.000đ
                                            </label>
                                            <label>
                                                <input type="radio"> Tặng Phiếu mua hàng 200.000đ
                                            </label>
                                            <p>Bạn chọn KM: Giảm ngay 200.000đ</p>
                                        </li>
                                        <li><i class="gift-green"></i>Trả góp 0% - 0đ trả trước - 0đ phí chuyển đổi đến hết ngày 09/04/2020</li>
                                        <li><i class="gift-green"></i>Miễn phí vận chuyển </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="thongso">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="video-info">
                            <h3>Tủ lạnh Sharp J-Tech Inverter | SharpViet.vn</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="product-parameter">
                        <div class="col-md-8">
                            <div class="parameter-image">
                                <img src="https://cdn.nguyenkimmall.com/images/thumbnails/696/522/companies/_1/HT19/Avatar/04_tulanh/10029435_TU-LANH_SHARP-INVERTER-253-LIT_SJ-X281E-SL.jpg" width="100%">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="parameter-table">
                                <table>
                                    <caption>Thông số kỹ thuật</caption>
                                    <tbody>
                                        <tr>
                                            <td class="title">Model:</td>
                                            <td class="value"> SJ-X281E </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Màu sắc:</td>
                                            <td class="value"> Bạc </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Nhà sản xuất:</td>
                                            <td class="value"> Sharp </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Xuất xứ:</td>
                                            <td class="value"> Thái Lan </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Thời gian bảo hành:</td>
                                            <td class="value"> 12 tháng </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Địa điểm bảo hành:</td>
                                            <td class="value"> Nguyễn Kim </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Kiểu tủ lạnh:</td>
                                            <td class="value"> Ngăn đá trên </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Số cửa tủ:</td>
                                            <td class="value"> 2 cửa </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Dung tích tủ lạnh:</td>
                                            <td class="value"> 253 </td>
                                        </tr>
                                        <tr>
                                            <td class="title">Dung tích ngăn đá:</td>
                                            <td class="value"> 60 lít </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="#">Xem thêm thông số kỷ thuật</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="product-feature">
                        <div class="col-md-12">
                            <h3>Bảo vệ tủ lạnh tối đa</h3>
                            <article>
                                <a href="#">Tủ lạnh Sharp SJ-X281E-SL 253 lít </a>được trang bị 7 tính năng bảo vệ an toàn gồm: Chống cháy, chống sấm sét, chống rung, chống chuột, vận hành ổn định, cách điện, kết cấu ổn định
                            </article>
                            <img src="https://cdn.nguyenkimmall.com/images/companies/_1/Content/dien-lanh/tu-lanh/Sharp/tu-lanh-sharp-sj-x281e-sl-1.jpg" alt="" width="100%">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="info-footer">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="info-comany">
                            <li class="title">Thông tin công ty</li>
                            <li><a href="#">Giới thiệu công ty</a></li>
                            <li><a href="#">Đối tác chiến lược</a></li>
                            <li><a href="#">Hệ thống trung tâm</a></li>
                            <li><a href="#">Tuyển dụng</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="policy">
                            <li class="title">Chính sách</li>
                            <li><a href="#">Giao nhận - lắp đặt</a></li>
                            <li><a href="#">Dùng thử sản phẩm</a></li>
                            <li><a href="#">Đổi trả sản phẩm</a></li>
                            <li><a href="#">Bảo hành</a></li>
                            <li><a href="#">Trả góp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="support">
                            <li class="title">Hỗ trợ khách hàng</li>
                            <li><a href="#">Hướng dẫn mua hàng</a></li>
                            <li><a href="#">Tra cứu bảo hành</a></li>
                            <li><a href="#">Hóa đơn điện tử</a></li>
                            <li><a href="#">Mua hàng doanh nghiệp</a></li>
                            <li><a href="#">Câu hỏi thường gặp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="contact-us">
                            <li class="title">Kết nối với chúng tôi</li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script>
        $('.category h3 i').click(function() {
            $('.nav-menu').toggleClass('active');
        });
        $(".mini img").click(function() {

            $(".maxi").attr("src", $(this).attr("src").replace("100x100", "400x400"));

        });
    </script>
</body>

</html>